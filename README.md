# Optimal enzyme profiles in unbranched metabolic pathways

## Abstract
How to optimize the allocation of enzymes in metabolic pathways has been a topic of study for many decades. Although the general problem is complex and non-linear, we have previously shown that it can be solved by convex optimization. In this paper, we focus on unbranched metabolic pathways with simplified enzymatic rate laws and derive analytic solutions to the optimization problem. We revisit existing solutions based on the limit of mass-action rate laws, and present new solutions for other rate laws. Furthermore, we revisit the known relationship between flux control coefficients and enzyme abundances in states of maximal enzyme efficiency. We generalize this relationship to models with density constrains on enzymes and metabolites, and present a new local relationship  between optimal enzyme amounts and reaction elasticities. Finally, we apply our theory to derive kinetics-based formulae for protein allocation during bacterial growth.

## Preprint
[bioRχiν](https://doi.org/10.1101/2023.06.30.547243)

## Getting started
```bash
git clone https://gitlab.com/elad.noor/optimal_enzyme_investment.git
pip install -r requirements.txt
jupyter notebook
```

## Authors and acknowledgment
This project was created by Elad Noor and Wolfram Liebermeister.

## License
MIT License
